﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    public int GoalsToWin = 5;
    public int CloseWarningDifference = 1;

    [Space()]
    public List<Vector3> startPositions = new List<Vector3>();
    public List<Goal> goals = new List<Goal>();
    private List<Character> characters = new List<Character>();

    private Dictionary<int, int> scores = new Dictionary<int, int>();

    [SerializeField]
    private Cinemachine.CinemachineTargetGroup cinemachineList;

    public void PlayerScoredGoal(int id)
    {
        int count;
        if (scores.TryGetValue(id, out count) == false)
        {
            scores[id] = 1;
        }
        else
        {
            scores[id] = count + 1;
        }

        count = scores[id];

        Debug.Log("Player " + id + " SCORED a goal!! total score: " + count + "\n");

        StartCoroutine(GoalFX());

        Interference i = GameManager.Instance.interference;
        if (i != null)
        {
            if (count != GoalsToWin)
                i.Remaining = 1.2f;
            else
                i.Remaining = i.length;
        }

        UIManager.instance.IncrementPlayerScore(id);
        Ball.Instance.Goal();

        if (count == GoalsToWin - CloseWarningDifference)
        {
            StartCoroutine(PlayerAlmostWinning(id));
        }
        else if(count >= GoalsToWin)
        {
            StartCoroutine(PlayerWon(id));
        }
    }
    public int GetScore(int id)
    {
        int count;
        if (scores.TryGetValue(id, out count) == true)
        {
            return count;
        }

        return 0;
    }
    public void ResetScores()
    {
        foreach (var i in scores.Keys)
        {
            scores[i] = 0;
        }
    }

    IEnumerator GoalFX()
    {
        AudioManager.Instance.Play("Goal_Explosion");
        yield return new WaitForSeconds(0.1f);

        AudioManager.Instance.Play("Goal");

        yield return new WaitForSeconds(0.15f);
        
        AudioManager.Instance.Play("Goal_Scored_VO");
    }

    IEnumerator PlayerAlmostWinning(int id)
    {
        Debug.LogError( id + " almost winning");

        GameManager.ForcePlayersImmobile(true);
        //AudioManager.Instance.Play("InterferenceShort");
        AudioManager.Instance.Play("AlmostWinning");


        // TODO: UI stuff here

        yield return new WaitForSeconds(0.5f);
        GameManager.ForcePlayersImmobile(true);
    }

    IEnumerator PlayerWon(int id)
    {
        Debug.LogError(id + " WON !!!!!");

        GameManager.ForcePlayersImmobile(true);

        // TODO: UI stuff here

        for(int i = 0; i < 2; i++)
        {
            yield return new WaitForSeconds(0.5f);
            AudioManager.Instance.Play("Goal");
        }

        yield return new WaitForSeconds(5f);
        GameManager.ForcePlayersImmobile(false);
        
        UnityEngine.SceneManagement.SceneManager.LoadScene("start_screen", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    void Start()
    {
        AudioManager.Instance.Play("Crowd_Bed");
        AudioManager.Instance.Play("MusicGame");
        Reset();
    }

    public void Reset()
    {
        if(characters.Count == 0)
        {
            for (int i = 0; i < 4; i++)
            {
                int prefabCount = GameManager.Instance.characterPrefabs.Count;
                GameObject prefab = GameManager.Instance.characterPrefabs[i % prefabCount];

                GameObject go = GameObject.Instantiate(prefab);
                go.name = "Character_" + i;
                go.transform.parent = this.transform;

                Character newChar = go.GetComponent<Character>();
                characters.Add(newChar);
            }
        }

        foreach (var g in FindObjectsOfType<Goal>())
        {
            if(goals.Contains(g) == false)
                goals.Add(g);
        }

        scores = new Dictionary<int, int>();
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < startPositions.Count; i++)
        {
            Gizmos.DrawWireSphere(startPositions[i] + transform.position, 0.5f);
        }
    }

    public void AssignCharacters(List<Player> players)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (i == startPositions.Count)
            {
                Debug.LogError("no more positions available");
                break;
            }

            if(i == characters.Count)
            {
                Debug.LogError("no more characters available");
                break;
            }

            if(i == goals.Count)
            {
                Debug.LogError("no more goals available");
                break;
            }
            
            Player p = players[i];
            p.Charater = characters[i];
            characters[i].Position = startPositions[i] + transform.position;

            Goal goal = goals[i];
            goal.Color = p.config.Color;
            goal.playerID = p.config.ID;
            goal.SetArena(this);
        }

        // Send list of characters to Cinemachine and set weights for camera zoom and follow
        InitializeCinemachine();
    }

    public void SwapPlayers()
    {
        List<Character> chars = GameManager.Instance.arena.GetCharacters();
        
        Player root = chars[0].ParentPlayer;
        for (int i = 0; i < chars.Count; i++)
        {
            Player p = null;

            if (i < chars.Count - 1)
                p = chars[i + 1].ParentPlayer;
            else
                p = root;

            p.Charater = chars[i];
        }
    }

    public List<Character> GetCharacters()
    {
        return characters;
    }

    public Character GetCharacter(int id)
    {
        return characters[id];
    }

    private void InitializeCinemachine()
    {
        if(cinemachineList is Cinemachine.CinemachineTargetGroup)
        {
            Transform ball = GameObject.Find("Ball").transform;
            Cinemachine.CinemachineTargetGroup.Target[] targets = new Cinemachine.CinemachineTargetGroup.Target[characters.Count + 1];
            float targetWeight = 1.0f / (characters.Count + 1);
            targets[0] = new Cinemachine.CinemachineTargetGroup.Target();
            targets[0].weight = targetWeight;
            targets[0].target = ball;
            targets[0].radius = 1;
            for (int i = 1; i < characters.Count + 1; i++)
            {
                targets[i] = new Cinemachine.CinemachineTargetGroup.Target();
                targets[i].weight = targetWeight;
                targets[i].target = characters[i - 1].transform;
                targets[i].radius = 2;
            }
            cinemachineList.m_Targets = targets;
        }
    }
}
