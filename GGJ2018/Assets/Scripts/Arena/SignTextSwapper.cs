﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignTextSwapper : MonoBehaviour {
    private TextMesh text;
    private List<string> signText;

	// Update is called once per frame
	void Start () {
        text = GetComponentInChildren<TextMesh>();
        CreateSignStrings();
        InvokeRepeating("UpdateSignText", 1f, 5f);
	}

    private void CreateSignStrings()
    {
        signText = new List<string>();
        signText.Add("Hello World");
        signText.Add("Drink 40w Oil");
        signText.Add("Go Sports!");
        signText.Add("3:1 Odds");
        signText.Add("DIV0 ERROR");
        signText.Add("Go Red!");
        signText.Add("Go Blue!");
        signText.Add("Go Green!");
        signText.Add("Go Yellow!");
    }

    void UpdateSignText()
    {
        SetSignText(signText[(int)Random.Range(0, signText.Count - 1)]);
    }

    private void SetSignText(string signText)
    {
        text.text = signText;
    }
}
