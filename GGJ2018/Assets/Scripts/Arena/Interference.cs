﻿using System.Collections;
using UnityEngine;

public class Interference : MonoBehaviour
{
    public bool InterferrenceEnabled = false;
    [HideInInspector]
    public float level = 0f;

    [Range(0f, 30f)]
    public float baseTime = 10f;
    [Range(0f, 10f)]
    public float variation = 3f;
    [Range(2f, 10f)]
    public float length = 2f;
    [Range(0f, 1f)]
    public float switchLevel = 0.5f;
    
    public float Remaining
    {
        get
        {
            return target - timer;
        }
        set
        {
            timer = target - value;
            Tick();
        }
    }

    private float timer = 0f;
    private float target = 10f;
    
    public void Tick()
    {
        if (InterferrenceEnabled == false)
        {
            level = timer = 0f;
            return;
        }
        
        timer += Time.deltaTime;
        level = Mathf.Clamp(Mathf.InverseLerp(target, target + length, timer), -0.1f, 1f);

        if (level > 0f && soundTriggered == false)
            StartCoroutine(TriggerSound());

        if (level >= switchLevel)
            TriggerSwitch();

        if (level >= 1f || timer >= target + length)
            Reset();
    }

    bool soundTriggered = false;
    private IEnumerator TriggerSound()
    {
        Debug.Log("interference sound");

        soundTriggered = true;
        for(int i = 0; i < 4; i++)
        {
            AudioManager.Instance.Play("Interference");
            yield return new WaitForSeconds(0.1f);
        }
        
        yield return null;
    }

    private bool switchHappened = false;
    private void TriggerSwitch()
    {
        if (switchHappened)
            return;

        switchHappened = true;
        GameManager.Instance.arena.SwapPlayers();
    }

    private void Reset()
    {
        float sine = Mathf.Clamp(Mathf.Sin(Time.time * 0.2f), -1, 1);
        target = Mathf.Clamp(baseTime + (variation / 2 * sine), 4f, 20f);
        level = 0f;
        timer = 0f;
        switchHappened = false;
        soundTriggered = false;
    }
}
