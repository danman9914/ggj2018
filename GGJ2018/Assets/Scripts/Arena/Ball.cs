﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    static private Ball _inst;
    static public Ball Instance { get { return _inst; } }

    [SerializeField]
    private GameObject hitEffectPrefab;

    [SerializeField]
    private GameObject goalEffectPrefab;

    [SerializeField]
    private Material defaultMat;

    [SerializeField]
    private Material hitMat;

    private Rigidbody rigid;

	void Awake ()
    {
        _inst = this;
        rigid = this.GetComponent<Rigidbody>();
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            Debug.Log("WallHit");
            AudioManager.Instance.Play("WallHit");
        }
    }

    public static void Reset()
    {
        _inst.rigid.velocity = _inst.transform.position = Vector3.zero;
    }

    public void Goal()
    {
        GoalEffect(this.transform.position);
        Reset();
    }

    public void AddForce(Vector3 dir)
    {
        AudioManager.Instance.Play("HitBall");

        rigid.AddForce(dir);
        if (dir.magnitude > 1f)
        {
            HitEffect(dir);
        }
    }

    private void HitEffect(Vector3 dir)
    {
        if (hitEffectPrefab != null)
        {
            var hitEffect = Instantiate(hitEffectPrefab);

            hitEffect.transform.position = new Vector3(transform.position.x, transform.position.y, -5f);
            hitEffect.transform.up = dir;

            Destroy(hitEffect.gameObject, 2.5f);

            StopAllCoroutines();
            StartCoroutine(FreezeFrame());
        }
    }

    private void GoalEffect(Vector3 pos)
    {
        var goalEffect = Instantiate(goalEffectPrefab);
        pos.z = -1;
        goalEffect.transform.position = pos;

        Destroy(goalEffect.gameObject, 1.5f);
    }

    private IEnumerator FreezeFrame()
    {
        GetComponent<Renderer>().material = hitMat;
        yield return new WaitForSecondsRealtime(0.05f);
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(0.05f);
        Time.timeScale = 1f;
        GetComponent<Renderer>().material = defaultMat;
    }
}
