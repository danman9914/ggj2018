﻿using UnityEngine;

public class Goal : MonoBehaviour
{   
    public int playerID = -1;
    public float radius = 1.5f;
    private Color color;
    public Color Color
    {
        set
        {
            color = value;
            if (spot != null)
                spot.color = color;
        }
    }
    public Light spot;

    private Arena arena;

    public void SetArena(Arena parentArena)
    {
        arena = parentArena;
    }

    private void OnTriggerEnter(Collider other)
    {
        Ball b = other.gameObject.GetComponent<Ball>();
        if(b != null)
        {
            arena.PlayerScoredGoal(playerID);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
