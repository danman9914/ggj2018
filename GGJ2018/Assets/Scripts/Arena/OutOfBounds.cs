﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour {
    public Arena arenaManager;

	void OnTriggerEnter (Collider c) {
        if (c.gameObject.layer == 8)
        {
            StopAllCoroutines();
            StartCoroutine(WaitForSeconds(c, 1));
        }
	}


    IEnumerator WaitForSeconds(Collider c, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        Rigidbody b = c.gameObject.GetComponent<Rigidbody>();
        b.velocity = Vector3.zero;
        b.angularVelocity = Vector3.zero;
        c.gameObject.transform.position = new Vector3(0, 0, -0.125f);

        arenaManager.AssignCharacters(GameManager.Instance.playerSystem.players);
    }
}
