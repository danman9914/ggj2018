﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField]
    private List<PlayerUI> playerUIs = new List<PlayerUI>();

    [SerializeField]
    private GoalSplash goalSplash;

    private void Awake()
    {
        instance = this;
    }

    public void SetControllersTransmitting(bool transmitting)
    {
        foreach (PlayerUI ui in playerUIs)
        {
            ui.SetTransmitting(transmitting);
        }
    }

    public void IncrementPlayerScore(int playerId)
    {
        playerUIs[playerId].IncrementScore();

        goalSplash.Play(GameColors.ColorForPlayer(playerId));
    }
}
