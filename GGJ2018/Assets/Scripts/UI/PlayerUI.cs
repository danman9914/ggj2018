﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour {

    [SerializeField]
    private TextMesh scoreText;

    private int score;

    private void Start()
    {
        DisplayScore();
    }

    public void SetTransmitting(bool transmitting)
    {
        GetComponent<Animator>().SetBool("transmitting", transmitting);
    }

    public void IncrementScore()
    {
        score++;
        DisplayScore();
    }

    private void DisplayScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }
}
