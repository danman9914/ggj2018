﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalSplash : MonoBehaviour {

    [SerializeField]
    private List<SpriteRenderer> tintedPieces = new List<SpriteRenderer>();

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Play(Color color)
    {
        AudioManager.Instance.Play("Goal_Overlay");

        foreach (SpriteRenderer sr in tintedPieces)
        {
            sr.color = color;
        }

        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }
}
