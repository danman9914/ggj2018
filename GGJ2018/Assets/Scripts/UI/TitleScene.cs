﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScene : MonoBehaviour
{
    private bool ending = false;

	private void EndScene()
    {
        if (!ending && Time.timeSinceLevelLoad > 2.5f)
        {
            ending = true;
            GetComponent<Animator>().SetTrigger("end");
            StartCoroutine(WaitThenLoadScene());
        }
    }

    AudioSource titleMusic = null;
    private IEnumerator PlaySoundDelayed()
    {
        yield return new WaitForSeconds(0.5f);
        AudioManager.Instance.Play("Start_Screen");
        AudioManager.Instance.Play("MusicTitle");
    }

    private IEnumerator WaitThenLoadScene()
    {
        yield return new WaitForSeconds(0.5f);
        AudioManager.Instance.Play("Start_Screen_Exit");
        yield return new WaitForSeconds(0.75f);

        AudioManager.Instance.Play("Goal");
        UnityEngine.SceneManagement.SceneManager.LoadScene("main");
    }

    private void Start()
    {
        StartCoroutine(PlaySoundDelayed());
    }

    void Update ()
    {
        if (Input.anyKeyDown)
        {
            if(titleMusic != null)
            {
                titleMusic.Stop();
                titleMusic = null;
            }

            EndScene();
        }
    }
}
