﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class SoundObject : ScriptableObject
{
    public bool loop = false;
    public List<AudioClip> clips;

    [Space()]
    [Range(-20, 0)]
    public float volume = 0f;
    [Range(-8, 0)]
    public float randomVolume = 0f;

    [Space()]
    [Range(-3,3)]
    public float pitch = 0f;
    [Range(-3, 0)]
    public float randomPitch = 0f;

    public UnityEngine.Audio.AudioMixerGroup bus;

    private void OnEnable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    public AudioSource Play(float volumeDB)
    {
        AudioSource s = GetSource();
        bool success = true;

        if (clips.Count <= 0)
        {
            Pool(s);
            return null;
        }

        s.clip = clips[Random.Range(0, clips.Count)];
        if (s.clip == null)
        {
            Pool(s);
            return null;
        }

        s.loop = loop;

        float volBase = Mathf.Clamp(volume + volumeDB, -20, 0);
        float db = volBase + Random.Range(randomVolume, 0f);
        s.volume = AudioManager.DecibelToLinear(db);

        float st = pitch + Random.Range(randomPitch, 0f);
        s.pitch = AudioManager.St2pitch(st);


        s.outputAudioMixerGroup = bus != null ? bus: sfxBus;

        s.Play();

        activeSources.Add(s);
        if(loop)
            return s;

        return null;
    }

    public static UnityEngine.Audio.AudioMixerGroup sfxBus;
    public static UnityEngine.Audio.AudioMixerGroup mxBus;
    private static List<AudioSource> availableSources = new List<AudioSource>();
    public static List<AudioSource> activeSources = new List<AudioSource>();
    private static GameObject audioRoot = null;
    private AudioSource GetSource()
    {
        if(audioRoot == null)
        {
            audioRoot = new GameObject();
            audioRoot.name = "AudioRoot";
            DontDestroyOnLoad(audioRoot);
        }

        AudioSource s = null;
        if (availableSources.Count > 0)
        {
            s = availableSources[0];
            availableSources.RemoveAt(0);
        }


        if(s == null)
        {
            GameObject go = new GameObject();
			go.name = "Audio_" + Random.Range (1000, 9999);
            go.transform.parent = audioRoot.transform;
            s = go.AddComponent<AudioSource>();
        }

        return s;
    }
    public static void Pool(AudioSource s)
    {
        s.Stop();

        if (availableSources.Contains(s) == false)
            availableSources.Add(s);
    }
}
