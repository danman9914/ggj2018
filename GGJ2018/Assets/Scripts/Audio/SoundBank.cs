﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu()]
public class SoundBank : ScriptableObject
{
    public List<SoundObject> sounds;

    public AudioMixerGroup mixerBus;

    private void OnEnable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }
}
