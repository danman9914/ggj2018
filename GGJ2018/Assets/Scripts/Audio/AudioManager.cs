﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }

    public SoundBank SFX;
    public SoundBank Music;
    private Dictionary<string, SoundObject> links = new Dictionary<string, SoundObject>();
    static private List<AudioSource> loops = new List<AudioSource>();

    private void Awake()
    {
        Instance = this;

        if (SFX == null)
        {
            Debug.LogError("sound bank missing!");
        }
        else
        {
            foreach (var s in SFX.sounds)
            {
                if (s != null)
                    links.Add(s.name, s);
            }
            SoundObject.sfxBus = SFX.mixerBus;
        }

        if (Music == null)
        {
            Debug.LogError("sound bank missing!");
        }
        else
        {
            foreach (var s in Music.sounds)
            {
                if (s != null)
                    links.Add(s.name, s);
            }
            SoundObject.mxBus = Music.mixerBus;
        }
    }

    public bool PlayKick = false;
    public bool PlayHitBall = false;
    public bool PlayGoal = false;
    public bool PlayInterference = false;

    private void LateUpdate ()
    {
        for (int i = SoundObject.activeSources.Count - 1; i >= 0; i--)
        {
            AudioSource s = SoundObject.activeSources[i];
            if(s == null)
            {
                SoundObject.activeSources.RemoveAt(i);
                continue;
            }

            if (s.isPlaying == false)
            {
                SoundObject.activeSources.RemoveAt(i);
                SoundObject.Pool(s);
            }
        }

        if (PlayKick)
        {
            PlayKick = false;
            Play("Kick");
        }

        if(PlayHitBall)
        {
            PlayHitBall = false;
            Play("HitBall");
        }

        if (PlayGoal)
        {
            PlayGoal = false;
            Play("Goal");
            Play("Goal_Scored_VO");
        }

        if (PlayInterference)
        {
            PlayInterference = false;
            Play("Interference");
        }
	}

    public AudioSource Play(string name, float volumeDB = 0)
    {
        SoundObject sound;
        if(links.TryGetValue(name, out sound) == false)
        {
            Debug.LogWarning("no sound found with name " + name);
            return null;
        }

        AudioSource instance = sound.Play(volumeDB);
        if(instance != null)
        {
            loops.Add(instance);
        }

        return instance;
    }

    public void StopLooping()
    {
        for(int i = loops.Count - 1; i >= 0; i--)
        {
            AudioSource s = loops[i];

            loops.RemoveAt(i);
            SoundObject.activeSources.Remove(s);
            SoundObject.Pool(s);
        }
    }

    public void StopAll()
    {
        for (int i = SoundObject.activeSources.Count - 1; i >= 0; i--)
        {
            AudioSource s = SoundObject.activeSources[i];
            
            SoundObject.activeSources.RemoveAt(i);
            SoundObject.Pool(s);
        }
    }

    public static float DecibelToLinear(float dB)
    {
        if (dB > -80)
            return Mathf.Clamp01(Mathf.Pow(10.0f, dB / 20.0f));
        else
            return 0;
    }

    private static float twelfthRootOfTwo = Mathf.Pow(2f, 1.0f / 12f);
    public static float St2pitch(float st)
    {
        return Mathf.Clamp(Mathf.Pow(twelfthRootOfTwo, st), 0f, 4f);
    }

}
