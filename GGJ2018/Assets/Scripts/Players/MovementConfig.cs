﻿using UnityEngine;

[CreateAssetMenu()]
public class MovementConfig : ScriptableObject
{
    public float MoveSpeed = 10f;
    public float KickThreshhold = 4f;
    public float KickForce = 100f;
    public float KickWaitTime = 0.25f;
    public WaitForSeconds kickWaiter = null;
}
