﻿using UnityEngine;

public class PlayerConfig
{
    public PlayerConfig(int id, string name)
    {
        ID = id;
        Name = name;

        Color = GameColors.ColorForPlayer(id);
    }

    public int ID = 0;
    public string Name = "Player";

    public Color Color;
}
