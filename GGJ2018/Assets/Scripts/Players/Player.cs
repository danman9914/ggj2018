﻿using UnityEngine;
using Rewired;
using System.Collections;

public class Player
{
    // settings
    public PlayerConfig config;

    private Character character;
    public Character Charater
    {
        get { return character; }
        set
        {
            character = value;
            
            character.ParentPlayer = this;
            character.Color = config.Color;
        }
    }
    
    public Player(PlayerConfig cnfg)
    {
        config = cnfg;
    }
}
