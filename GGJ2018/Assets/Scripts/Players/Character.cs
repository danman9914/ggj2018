﻿using UnityEngine;
using System.Collections;
using Rewired;

[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour
{
    public Player ParentPlayer = null;

    [SerializeField]
    private BoxCollider kickCollider;

    private Transform trans;
    private CharacterController cc;

    private bool immobile;
    private bool kicking;

    private CharacterAnimationController animController;
    private const float GROUND_Z_OFFSET = -1.5f;

    public Color Color
    {
        set
        {
            animController.SetColor(value);
        }
    }
    public Vector3 Position
    {
        get { return trans.position; }
        set { trans.position = value; }
    }

    void Awake()
    {
        trans = transform;
        cc = GetComponent<CharacterController>();
        animController = GetComponentInChildren<CharacterAnimationController>();
        kickCollider = GetComponentInChildren<BoxCollider>();
    }

    private void LateUpdate()
    {
        FixZPosition();
    }

    public void SetImmobile(bool immobile)
    {
        this.immobile = immobile;
        animController.SetImmobile(immobile);
    }

    public void Move(Vector3 moveVector)
    {
        if (!kicking)
        {
            cc.Move(moveVector);
            animController.SetRunning(!immobile && moveVector.magnitude > 0.1f);
        }
    }

    public void Rotate(Vector2 moveVector)
    {
        if (!immobile)
        {
            transform.up = moveVector.normalized;
        }
    }

    public void Kick()
    {
        if (!kicking && !immobile)
        {
            var ball = Ball.Instance;
            if (kickCollider.bounds.Intersects(ball.GetComponent<SphereCollider>().bounds))
            {
                ball.AddForce((trans.up).normalized * 1000f);
                Debug.Log("kicked it");
            }

            animController.Kick();
            StopAllCoroutines();
            StartCoroutine(WaitWhileKicking());
            AudioManager.Instance.Play("Kick");
        }
    }

    private void FixZPosition()
    {
        trans.position = new Vector3(trans.position.x, trans.position.y, GROUND_Z_OFFSET);
    }

    private IEnumerator WaitWhileKicking()
    {
        kicking = true;

        yield return new WaitForSeconds(GameManager.Instance.movement.KickWaitTime);

        kicking = false;
    }
}