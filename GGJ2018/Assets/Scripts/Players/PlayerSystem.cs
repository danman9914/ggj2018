﻿using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerSystem
{
    public List<Player> players = new List<Player>();
    public List<InputState> inputs = new List<InputState>();
    
    public List<Player> GetPlayers()
    {
        return players;
    }
    public List<InputState> GetInputs()
    {
        return inputs;
    }
    
    private static MovementConfig config;
    public PlayerSystem(MovementConfig movement)
    {
        config = movement;
    }
    
    public void UpdateInputs()
    {
        for (int i = 0; i < players.Count; i++)
        {
            InputState state = inputs[i];
            state.GetInput();
        }
    }

    public InputState GetInput(int id)
    {
        if(id > -1 && id < 4)
        {
            return inputs[id];
        }

        return null;
    }
}

public class InputState
{
    public Player parentPlayer;
    public Rewired.Player player;

    public Vector3 moveVector = Vector3.zero;
    public bool fire = false;
    public bool shoulder = false;

    private Controller controller;

    public void GetInput()
    {
        if (controller == null)
        {
            controller = ReInput.controllers.GetJoystick(parentPlayer.config.ID);
            player.controllers.AddController(controller, true);
        }

        moveVector.x = player.GetAxis("Move Horizontal");
        moveVector.y = player.GetAxis("Move Vertical");

        fire = player.GetButtonDown("Fire");
        shoulder = player.GetButtonDown("Shoulder");
    }

    public void Rumble(float intensity, float duration)
    {
        if (intensity <= 1.0f)
            player.SetVibration(0, intensity, duration);
    }
}

public class TransmissionSignal
{
    public float InterferrenceLevel = 0f;
}