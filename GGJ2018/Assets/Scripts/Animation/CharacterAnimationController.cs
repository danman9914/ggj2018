﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField]
    private List<Renderer> tintedPieces;

    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void SetColor(Color color)
    {
        foreach (Renderer r in tintedPieces)
        {
            var sr = r.GetComponent<SpriteRenderer>();
            if (sr != null)
            {
                sr.color = color;
            }
            else
            {
                r.material.color = color;
            }
        }
    }

    public void SetImmobile(bool immobile)
    {
        anim.SetBool("immobile", immobile);
    }

    public void SetRunning(bool running)
    {
        anim.SetBool("running", running);
    }

    public void Kick()
    {
        anim.SetTrigger("kick");
    }
}
