﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameColors
{
    public static readonly Color RED = new Color(0.9725f, 0.2f, 0.2353f);
    public static readonly Color BLUE = new Color(0.1059f, 0.7765f, 0.8980f);
    public static readonly Color GREEN = new Color(0.2314f, 0.8667f, 0.4510f);
    public static readonly Color YELLOW = new Color(0.9882f, 0.6706f, 0.0627f);

    public static Color ColorForPlayer(int playerIndex)
    {
        switch (playerIndex)
        {
            default:
                return RED;
            case 1:
                return BLUE;
            case 2:
                return YELLOW;
            case 3:
                return GREEN;
        }
    }
}
