﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTester : MonoBehaviour
{
#if UNITY_EDITOR
    void Update ()
    {
        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<CharacterAnimationController>().SetRunning(true);
        }
        else
        {
            GetComponent<CharacterAnimationController>().SetRunning(false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            GetComponent<CharacterAnimationController>().Kick();
        }

        if (Input.GetMouseButton(0))
        {
            transform.up = ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector2)transform.position).normalized;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GetComponent<CharacterAnimationController>().SetColor(GameColors.RED);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GetComponent<CharacterAnimationController>().SetColor(GameColors.BLUE);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GetComponent<CharacterAnimationController>().SetColor(GameColors.YELLOW);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            GetComponent<CharacterAnimationController>().SetColor(GameColors.GREEN);
        }
    }
#endif
}
