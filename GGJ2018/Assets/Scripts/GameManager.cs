﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR_OSX
using XboxCtrlrInput;
#endif

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager Instance
    { get { return _instance; } }
    
    [Space()]
    public List<GameObject> characterPrefabs = new List<GameObject>();
    public GameObject goalPrefab;
    public MovementConfig movement;

    [Space()]
    public Material goalPostMaterial;

    [Space()]
    public Arena arena;
    public PlayerSystem playerSystem;
    public Interference interference;

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this);
            return;
        }

        _instance = this;

        interference = GetComponent<Interference>();

        if (arena == null)
            arena = FindObjectOfType<Arena>();

        arena.Reset();

        playerSystem = new PlayerSystem(movement);

        while (Rewired.ReInput.isReady == false)
        {
            // wait
        }

        for(int i = 0; i < 4; i++)
        {
            // config setup
            PlayerConfig config = new PlayerConfig(i, "Player_" + i);

            // player setup
            Player player = new Player(config);
            playerSystem.players.Add(player);

            // input setup
            InputState input = new InputState
            {
                parentPlayer = player,
                player = Rewired.ReInput.players.GetPlayer(config.ID)
            };
            input.GetInput();
            playerSystem.inputs.Add(input);
        }

        arena.AssignCharacters(playerSystem.players);
    }

    private bool ForceImmobile = false;
    void Update ()
    {
        interference.Tick();

        playerSystem.UpdateInputs();

        ApplyMovement();
    }

    public void ApplyMovement()
    {
        List<Player> players = playerSystem.GetPlayers();
        List<InputState> inputs = playerSystem.GetInputs();

        float speed = movement.MoveSpeed * Time.deltaTime;
        for (int i = 0; i < players.Count; i++)
        {
            InputState state = inputs[i];

#if UNITY_EDITOR_OSX
			bool right = XboxCtrlrInput.XCI.GetDPad(XboxCtrlrInput.XboxDPad.Right);
			bool left = XboxCtrlrInput.XCI.GetDPad(XboxCtrlrInput.XboxDPad.Left);
			bool up = XboxCtrlrInput.XCI.GetDPad(XboxCtrlrInput.XboxDPad.Up);
			bool down = XboxCtrlrInput.XCI.GetDPad(XboxCtrlrInput.XboxDPad.Down);

			float x = 0;
			float y = 0;

			if(right)
				x = 1;
			else if (left)
				x = -1;

			if(up)
				y = 1;
			else if (down)
				y = -1;

			Vector2 move = new Vector2
			(
					x, y
			);

            bool fire = XboxCtrlrInput.XCI.GetButtonDown(XboxCtrlrInput.XboxButton.A);
            if(fire == false)
                fire = XboxCtrlrInput.XCI.GetButtonDown(XboxCtrlrInput.XboxButton.RightBumper);

			state.moveVector = move;
            state.fire = fire;
#endif
            Character ch = players[i].Charater;
            if (ch == null)
                return;

            if (state.fire)
            {
                ch.Kick();
            }

            bool playerImmobile = EvaluateInterference(inputs[i]);
            if (ForceImmobile)
                playerImmobile = true;

            UIManager.instance.SetControllersTransmitting(playerImmobile);

            // Process movement
            ch.SetImmobile(playerImmobile);
            ch.Move(state.moveVector * speed);

            if (state.moveVector.x != 0.0f || state.moveVector.y != 0.0f)
            {
                ch.Rotate(state.moveVector);
            }
        }
    }

    private bool EvaluateInterference(InputState input)
    {
        bool playerImmobile = false;
        if (interference.level > 0f)
        {
            Vector3 randomMovement = Random.insideUnitSphere * interference.level;

            if (interference.level > interference.switchLevel / 2)
            {
                input.moveVector = randomMovement;
                playerImmobile = true;
            }
            else
                input.moveVector += randomMovement;

            input.moveVector.z = 0f;
            input.Rumble(interference.level, 0.2f);
        }

        return playerImmobile;
    }

    public static void ForcePlayersImmobile(bool value)
    {
        Instance.ForceImmobile = value;
    }

    public static void RumblePlayers(float length, float intensity)
    {
        foreach (var i in Instance.playerSystem.GetInputs())
        {
            i.Rumble(intensity, length);
        }
    }
    
    private void OnGUI()
    {
        if(GUILayout.Button("Go To STart Screen"))
        {
            AudioManager.Instance.StopAll();
            UnityEngine.SceneManagement.SceneManager.LoadScene("start_screen", UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        if (GUILayout.Button("Reset Game"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("main", UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}
